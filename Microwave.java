public class Microwave{
	
	public String size;
	public int watts;
	public boolean smart;
	public String color;

	public void isItSmart(){
		if(this.smart==true){
			System.out.println("This is a smart microwave");
		}else{
			System.out.println("This is a normal microwave");
		}
	}
	
	public String getSize(){
		return this.size;
	}
}

