import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		Microwave[] mw=new Microwave[4];
		
		for(int i=0;i<mw.length;i++){
			
			mw[i]=new Microwave();
			
			System.out.println("Enter the size of the microwave");
			mw[i].size=sc.nextLine();
			
			System.out.println("Enter the color");
			mw[i].color=sc.nextLine();
			
			System.out.println("Enter true or false if it is a smart microwave");
			mw[i].smart=sc.nextBoolean();
			
			System.out.println("Enter the watts");
			mw[i].watts=sc.nextInt();

			//to move the pointer
			sc.nextLine();
			
		}
		
		//print last microwave's fields
		System.out.println("Microwave's size field: "+mw[mw.length-1].size);
		System.out.println("Microwave's color field: "+mw[mw.length-1].color);
		System.out.println("Microwave's smart field: "+mw[mw.length-1].smart);
		System.out.println("Microwave's watts field: "+mw[mw.length-1].watts);
		
		//2 instance methods on the first microwave
		System.out.println("First method isItSmart() output: ");
		mw[0].isItSmart();
		System.out.println("Second method getSize() output: ");
		System.out.println(mw[0].getSize());
		
	}

}